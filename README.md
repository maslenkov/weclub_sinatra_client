# Doorkeeper Sinatra Client Example for WeClub integration

NOTE: This app is a fork of original [doorkeeper-sinatra-client](https://github.com/doorkeeper-gem/doorkeeper-sinatra-client) with tiny updates for our needs.

This app is an example of OAuth 2 client. It was built in order to test the [doorkeeper provider example](http://doorkeeper-provider.herokuapp.com/). It uses [oauth2](https://github.com/intridea/oauth2) and [sinatra](http://www.sinatrarb.com/) gems. *But it is forked in order to test the weclub developer feature.* The original source code is, as always, [available on GitHub](https://github.com/applicake/doorkeeper-sinatra-client).

## About Doorkeeper Gem

For more information [about the gem](https://github.com/applicake/doorkeeper), [documentation](https://github.com/applicake/doorkeeper#readme), [wiki](https://github.com/applicake/doorkeeper/wiki/_pages) and another resources, check out the project [on GitHub](https://github.com/applicake/doorkeeper).

## Installation

First clone the [repository from GitHub](https://github.com/applicake/doorkeeper-sinatra-client):

    git clone git://github.com/applicake/doorkeeper-sinatra-client.git

Install all dependencies with:

    bundle install

## Configuration

### Client application

Firstly you have to create a new client for this application on weclub or its local copy. Make sure to append the `/callback` to the `redirect uri` (e.g. `http://localhost:9393/callback`).

### Environment variables

You need to setup few environment variables in order to make the client work. You can either set the variables in you environment:

    export OAUTH2_CLIENT_ID           = "129477f..."
    export OAUTH2_CLIENT_SECRET       = "c1eec90..."
    export OAUTH2_CLIENT_REDIRECT_URI = "http://localhost:9393/callback"
    ; export CITE                     = "http://localhost:3000"


or set them in a file named `env.rb` in the app's root. This file is loaded automatically by the app.

    # env.rb
    ENV['OAUTH2_CLIENT_ID']           = "129477f..."
    ENV['OAUTH2_CLIENT_SECRET']       = "c1eec90..."
    ENV['OAUTH2_CLIENT_REDIRECT_URI'] = "http://localhost:9393/callback"
    # ENV['CITE']                     = "http://localhost:3000"

## Start the server

Fire up the server with:

    rackup config.ru
